<?php
namespace Paygate\Model;
/**
* 
*/
class Database {

	function __construct(){
		try{
			$this->db = NewADOConnection('mysqli');
			$this->db->Connect( HOSTNAME, USERNAME, PASSWORD, DATABASE);
			\ADOdb_Active_Record::SetDatabaseAdapter($this->db);
		} catch (Exception $e){
			var_dump($e);
		}
	}
}

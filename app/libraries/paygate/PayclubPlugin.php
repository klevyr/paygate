<?php
namespace Paygate;
use Paygate\PlugInClientSend;
use Paygate\PlugInClientRecive;
use Paygate\TripleDESEncryption;

/**
PaygateSend :: Dinersclub
Clase destinada al control del plugin del 
boton de pagos para el envio y generacion
del formulario de invocacion del boton de
pagos.
 */
class PaygateSend extends PlugInClientSend {
	var $merchantId = null;
	var $adquirerId = null;
	var $urlVPOS = null;
	var $xmlVPOS = null;
	
	
	function __construct(){
		#llaves
		$this->setIV( Config::VECTORINI );
		$this->setSignPrivateKey( Config::CCPRIVSIGN );
		$this->setCipherPublicKey( Config::PCPUBCIPHER );
		#param form fields
		$this->setMerchantId( Config::MERCHANTID );
		$this->setAdquirerId( Config::ADQUIRERID );
		#param plugin
		$this->setLocalID( Config::LOCALID );
		$this->setCurrencyID( Config::CURRENCYID );
		$this->setSourceDescription( Config::URLTEC );
		#param payclub urls
		$this->setUrlVPOS( Config::URLVPOS );
		$this->setXmlVPOS( Config::XMLVPOS );

	}

	function setParamametros($param){
		foreach ($param as $k => $v) {
			switch ($k){
				case 'trxid': $this->setTransacctionID($v); break;
				case 'subt': $this->setTransacctionValue($v); break;

				case 'tax1': $this->setTaxValue1($v); break;
				case 'tax2': $this->setTaxValue2($v); break;
				case 'tip': $this->setTipValue($v); break;
				case 'ref1': $this->setReferencia1($v); break;
				case 'ref2': $this->setReferencia2($v); break;
				case 'ref3': $this->setReferencia3($v); break;
				case 'ref4': $this->setReferencia4($v); break;
				case 'ref5': $this->setReferencia5($v); break;
			}
		}
		return $this;
	}
	
	function getFormFields(){
		//Generate SING
		$XMLGKey = $this->CreateXMLGENERATEKEY();
		$this->XMLProcess();
		
		//Fields
        $formFields['XMLREQUEST']     = $this->getXMLREQUEST();
        $formFields['TXTREQUEST']     = $this->getTXTREQUEST();
		$formFields['TRANSACCIONID']	= $this->getTransacctionID();
        $formFields['XMLDIGITALSIGN'] = $this->getXMLDIGITALSIGN();
        $formFields['XMLACQUIRERID']  = $this->getMerchantId();
        $formFields['XMLMERCHANTID']  = $this->getAdquirerId();
        $formFields['XMLGENERATEKEY'] = $XMLGKey;
		
		return $formFields;
	}
	// setters & getters	
	function setMerchantId($value) { $this->merchantId = $value; }
	function getMerchantId() { return $this->merchantId; }
	
	function setAdquirerId($value) { $this->adquirerId = $value; }
	function getAdquirerId() { return $this->adquirerId; }

	function setUrlVPOS($value) { $this->urlVPOS = $value; }
	function getUrlVPOS() { return $this->urlVPOS; }

	function setXmlVPOS($value) { $this->xmlVPOS = $value; }
	function getXmlVPOS() { return $this->xmlVPOS; }

}

/**
PaygateReturn :: Dinersclub
Clase destinada al control del plugin del 
boton de pagos para la recepcion de informacion
originada por el boton de pagos.
 */
class PaygateReturn extends PlugInClientRecive {
	var $urlVPOS = null;
	var $xmlVPOS = null;
	var $response = null;
	var $arrMarcas = null;
	
	function __construct(){
		#llaves
		$this->setIV( Config::VECTORINI );
		$this->setSignPublicKey( Config::PCPUBSIGN );
		$this->setCipherPrivateKey( Config::CCPRIVCIPHER );
		#param marcas
		$this->arrMarcas = array(
			'DN' => 'DinersClub',
			'DI' => 'Discover',
			'VI' => 'Visa',
			'MC' => 'Mastercard',
			'' => 'N/D'
		);
		#param plugin
		$this->setLocalID( Config::LOCALID );
		$this->setCurrencyID( Config:: CURRENCYID );
		$this->setSourceDescription( Config::URLTEC );
		#param payclub urls
		$this->setUrlVPOS( Config::URLVPOS );
		$this->setXmlVPOS( Config::XMLVPOS );

	}
	
	function setResponsePayment($post){
		$this->response = $post;
		return $this;
	}
	
	function getPaymentStatus(){
		$_error = $this->setXMLGENERATEKEY( $this->response['XMLGENERATEKEY'] );
		if ($_error != "") {
            $status = false;
            throw new Exception('Incorrect XMLGK: <br />'. $this->response['XMLGENERATEKEY'] , 40);
        }
		try {
			$paymentStatus = $this->XMLProcess($this->response['XMLRESPONSE'], $this->response["XMLDIGITALSIGN"]);
			if($paymentStatus != 1) { throw new Exception('Datos Alterados. ', 40); }
		}
		catch(Exception $e) {
            throw new Exception('Información recibida no corresponde a Diners. <br>' . $e->getMessage(), 40);
		}
		return $this;
	}
	
	function setUrlVPOS($value) { $this->urlVPOS = $value; }
	function getUrlVPOS() { return $this->urlVPOS; }

	function setXmlVPOS($value) { $this->xmlVPOS = $value; }
	function getXmlVPOS() { return $this->xmlVPOS; }
	
}

/**
PaygatePosproc :: Dinersclub
Clase destinada al control del plugin del 
boton de pagos para la recepcion y decodificacion
de los datos recibidos en la pagina de porproceso
 */
class PaygatePosproc extends TripleDESEncryption {
	var $iniVector=null;
	var $simetricKey=null;
	var $merchantId=null;
	
	function __construct(){
		#llaves
		$this->setIV( Config::VECTORINI );
		$this->setSimetricKey( Config::SIMETRICKEY );
		#datos
		$this->setMerchantId( Config::MERCHANTID );
	}
	
	function getXMLReq($postdata){
		$_post = urldecode($postdata);
		$output=null;
		$querystring = $this->decrypt($_post, $this->getSimetricKey(), $this->getIV());
		parse_str($querystring, $output);
		return $output;
	}
	
	function setIV($iv){ $this->iniVector = $iv; }
	function getIV(){ return $this->iniVector; }
	
	function setSimetricKey($sk){ $this->simetricKey = $sk; }
	function getSimetricKey(){ return $this->simetricKey; }
	
	function setMerchantId($value) { $this->merchantId = $value; }
	function getMerchantId() { return $this->merchantId; }
}

/**
PaygateXmlQuery :: Dinersclub
Clase destinada para el control de la consulta XML
 */
class PaygateXmlQuery {
	var $xmlUrl = null;
	var $merchantId = null;
	
	function __construct(){
		$this->xmlUrl = Config::XMLVPOS;
		$this->merchantId = Config::MERCHANTID;
	}
	
	function xmlQueryManual($order)
	{
		// set url var
		$url = $this->xmlUrl . '?RucEstab=' . $this->merchantId . '&NoTransaccion=' . $order;
		
		// open connection
		$ch = curl_init();
	
		// set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		// execute post
		$result = simplexml_load_string(curl_exec($ch));
		// close connection
		curl_close($ch);

		return $result;
	}
}

/**
PaygateConfig :: Dinersclub
Clase para almacenamiento de datos de configuracion
del boton de pagos.
 */
class Config {
const 
	/** Parametro Paygate RUC Comercio*/
	MERCHANTID 		= '1792550165001',
	/** Parametro Paygate RUC COmercio / Url Tecnica*/
	ADQUIRERID 		= '1792550165001',
	/** Codigo de localidad */
	LOCALID 		= 'GN01',
	/** URL de invocacion pasarela de pago payclub */
	URLVPOS 		= 'https://www.optar.com.ec/webmpi/vpos',
	/** URL de consulta XML de payclub */
	XMLVPOS 		= 'https://www.optar.com.ec/webmpi/qvpos',
	/** URL Tecnica */
	URLTEC 			= 'https://cuponcity.ec/generar-transaccion/0/payclub',
	/** Cod. de moneda 840=Dollar */
	CURRENCYID 		= '840',
	/** Parametro Ventos de Inicializacion */
	VECTORINI 		= 'LopctjZ9vA0=',
	/** Llave simetrica */
	SIMETRICKEY 	= 'DoAfwgTc+HX9MdoVE9bfg4yizkkV0xxX',
	/** Paygate: Cifrado Publico */
	PCPUBCIPHER 	= '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCOHGDnebqk2D9onuaMmN7wi6G8
Gv4rRi4sY/LqSOyNbk0cMVhzt2yTM48eb0amjax1IPMQ9pexFWUsgVNqxOg6U4un
jGS4+9ID0AGhaC4yYNcWy/f/8z2GjT/+rFJL928pNarepu/uGYNypgR+cQXEZEUT
f/61UWaUZrY4um3F7wIDAQAB
-----END PUBLIC KEY-----',
	/** Paygate: Firma Publica */
	PCPUBSIGN 		= '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC5dJZ7CWeI9r4qmhGapKYQOuZt
ZCZzpIkb9JZbKOYayQwUk5hefeUC2NHKxliKWr3vaVP/DI6uFNMtAHZdiab9RUoW
bthLGDs+zJely2j20FoaZvrG3/y/wAcEaElklM0PJQyqU/wc5AJIMqakskwJukr7
txStm+wiaEUwFa1g7wIDAQAB
-----END PUBLIC KEY-----',
	/** Cuponcity: Cifrado Privado */
	CCPRIVCIPHER 	= '-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQDDfu5l/0ViU+zC0o6NNjfvsS904RB43l5sQqQ4jsvCcTGnkzb+
2N4V4BJQw+xTBrIBFN3xL46GxcOE2qcM0C+unuoVXM3a4XVXdsI+HI4Inm88G4HV
DPkcu0BCukvJVne3dB3DyDWsD0lHf/lfizow0/i8SH+7miQGh8Op3Wg2jQIDAQAB
AoGAK9EqW+AgTA6ldXE+FDZ/yRB8o4TCbWTh4v/40NhooRc/2uBrJimzdVG4qTOQ
p3dLtGRZ7nILTvfKf9+GK6AJ34RPm/rZFM8Ks4ilbtfI3nMIQ8YYaAvNUUOBIccr
YVJEwQRTQwxjLvHBy3oEgOubtsByYQEPRNlY+cOR9VG/f10CQQDo122U4mdNqQp6
+qbzrMf6I/S7dS6+150a/muwKv6auzzejiV2v8mTV5m8JBbXZZzhJbgjxY+RGw1j
TiiDticjAkEA1vCdCY1zZqIY9HFq+dRMpsGiwupU19K9f6AEGmcxSS4+cFR2FUKW
mojlw7Gt/fm6aMrMu3CcKAZtQXbL/+jejwJAC81vto5LYjphV56A+/UUJe/Qpffo
qK/RPN8Mol9rjHhVO4rFB2HtW9Si7FB//+ccrqT00otfHaHbad86WoLPNQJBAKb7
MuTS8U5QrBZrbnI8TnhWgn3u0o8A95sI02c3uABkXrmhxELsAlh39hg8FzHyKrFy
q+8KoyYtJ//trLpZTV0CQQCsF4p4//4qeiDjWisnAu3yCScetv7vINENVywWziEe
2/Y2adf8KJPifBJIB8VtTKFZjtW/ONpoSAAHe0GhDfnL
-----END RSA PRIVATE KEY-----',
	/** Cuponcity: Cifrado Publico */
	CCPUBCIPHER 	= '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDfu5l/0ViU+zC0o6NNjfvsS90
4RB43l5sQqQ4jsvCcTGnkzb+2N4V4BJQw+xTBrIBFN3xL46GxcOE2qcM0C+unuoV
XM3a4XVXdsI+HI4Inm88G4HVDPkcu0BCukvJVne3dB3DyDWsD0lHf/lfizow0/i8
SH+7miQGh8Op3Wg2jQIDAQAB
-----END PUBLIC KEY-----',
	/** Cuponcity: Firma Privado */
	CCPRIVSIGN 		= '-----BEGIN RSA PRIVATE KEY-----
MIICXgIBAAKBgQDjWHPCpzoF0CKgT2W9xPeBpbjAm1iwRKqjHehEUV/aMBC87y7d
72SH0B9HhwQ4+F2L7pkLW7J8SA1FJW96Ty9ucKmX5PE/gvuHOgluZYQOKQvtPUmx
+TLA2TQA9/fcJgzkMkC51dH84nOQVaTP28Q3OZlVMIiZHgBssAxS/PAzKwIDAQAB
AoGAWebUgBkY/Mt9pd8/4HdPWNlr4d6Mh8ffonllK/7x5Jo1REFhSyormvlWJTbj
GJQ+ieNkafVs83CarslK+BbwLgEeJ0MhkdIWiDcc27VtGrTi7uWZwh1eFw3dh5HW
PV+ADFalF8g51OujbS5S6DKHi4jxd/Nrke03KZRKXmVYSgECQQD9glsaHyfshEM6
fh28CXQ4T54ZLZToyZuLWpAx3vidSELMqCQMASl66mEEEm3M+EUwHCxYXqCNCAzS
3gvFHt3rAkEA5ZRJnv5MBh/DQEEzozA86Kisdrk3leGTMCt+SxPg6MTUYW7gLUqr
md7/VV020TMXc0r4WqNg/U1WdykddNZvwQJBALpRn3FwaqBbiSmpXWNBM4jbg2AP
7zy0SDzAf/AXZ3P5kxblkTC9feYX8tvSS5707azxV+pPCgL9YqdsiQwEpqcCQQC3
naJER58GRuEiwwNJwYr1ifJmAj0y4veVzzNzWLLJeKNkSgmCL/aPWotc+vCc4QAg
dhWsaH9qGDDPnVKx39ABAkEA4xVwSpGfobAT03P/q+IGnvXDlczE9UO4M1nlyElJ
nlw0OXVA9I1eSw3MkJ9t07MHy/qACEe9hMfF3Jba3EUQtg==
-----END RSA PRIVATE KEY-----',
	/** Cuponcity: Firma Publica */
	CCPUBSIGN 		= '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDjWHPCpzoF0CKgT2W9xPeBpbjA
m1iwRKqjHehEUV/aMBC87y7d72SH0B9HhwQ4+F2L7pkLW7J8SA1FJW96Ty9ucKmX
5PE/gvuHOgluZYQOKQvtPUmx+TLA2TQA9/fcJgzkMkC51dH84nOQVaTP28Q3OZlV
MIiZHgBssAxS/PAzKwIDAQAB
-----END PUBLIC KEY-----';

}
